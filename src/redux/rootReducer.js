import {all} from "redux-saga/effects";
import {combineReducers} from "redux";

import * as auth from "../app/modules/Auth/_redux/authRedux";
import {productsSlice} from "../app/modules/Posts/_redux/products/productsSlice";
import {commentsSlice} from "../app/modules/Comments/_redux/comments/commentsSlice";

export const rootReducer = combineReducers({
  auth: auth.reducer,
  products: productsSlice.reducer,
  comments: commentsSlice.reducer,
});

export function* rootSaga() {
  yield all([auth.saga()]);
}
