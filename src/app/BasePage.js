
import React, {useEffect,Suspense, lazy} from "react";
import { useDispatch } from "react-redux";
import {useHistory,Redirect, Switch, Route} from "react-router-dom";
import {LayoutSplashScreen} from "../_metronic/layout";

import * as RouteHelpers from '../_metronic/_helpers/RouterHelpers';
import * as auth from './modules/Auth/_redux/authRedux';

const Posts = lazy(() =>
  import("./modules/Posts/pages/PostsPage")
);
const Comments = lazy(() =>
  import("./modules/Comments/pages/CommentsPage")
);

export default function BasePage(props) {
    let urlParams = props.urlParams
    const dispatch = useDispatch();
    const history = useHistory();

    const update_token = async (refresh_token) =>{
        await dispatch(auth.actions.setRefreshToken(refresh_token))

        history.push("/posts/all")

    } 

    useEffect(() => {
        if(urlParams.hasOwnProperty('RefreshToken') && urlParams.RefreshToken!==""){
            update_token(urlParams.RefreshToken);
        }
    }, []) // [] - is required if you need only one call


    return (
        <Suspense fallback={<LayoutSplashScreen/>}>
            <Switch>
                {
                    /* Redirect from root URL to /dashboard. */
                    <Redirect exact from="/" to="/posts"/>
                }
                <Route path="/posts" component={Posts}/>
                <Route path="/comments" component={Comments}/>
                {/* <Redirect to="error/error-v1"/> */}
            </Switch>
        </Suspense>
    );
}
