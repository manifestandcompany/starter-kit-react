/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * components (e.g: `src/app/modules/Auth/pages/AuthPage`, `src/app/BasePage`).
 */

import React,{useEffect} from "react";
import { useHistory,Redirect, Switch, Route } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import {Layout} from "../_metronic/layout";
import BasePage from "./BasePage";
import { Logout, AuthPage } from "./modules/Auth";
import * as RouteHelpers from '../_metronic/_helpers/RouterHelpers';
import ErrorsPage from "./modules/ErrorsExamples/ErrorsPage";


export function Routes() {
    const history = useHistory();
    let urlParams = RouteHelpers.get_uri_params(history.location)
    const {isAuthorized} = useSelector(
        ({auth}) => ({
            isAuthorized: auth.user != null
        }),
        shallowEqual
    );
    useEffect(() => {
    }, []) // [] - is required if you need only one call

    return (
        <Switch>
            <Route path="/error" component={ErrorsPage}/>
            
            <Route path="/logout" component={Logout}/>


            {!isAuthorized && !Object.keys(urlParams).length ? (
                /*Redirect to `/auth` when user is not authorized*/
                <Redirect to="/auth/login"/>
            ) : (
                <Layout>
                    <BasePage urlParams={urlParams} />
                </Layout>
            )}
        </Switch>
    );
}
