import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { put, takeLatest } from "redux-saga/effects";
import { getUserByToken, GetPermissions } from "./authCrud";
import jsonwebtoken from 'jsonwebtoken'
import config from '../../../../config'


export const actionTypes = {
  Login: "[Login] Action",
  Logout: "[Logout] Action",
  Register: "[Register] Action",
  UserRequested: "[Request User] Action",
  UserLoaded: "[Load User] Auth API",
  GetPermissions: "[Get User Permissions] Auth API",
  setRefreshToken:"[Set Refresh Token] Auth API"
};

const initialAuthState = {
  user: undefined,
  authToken: undefined,
  RefreshToken: undefined
};

export const reducer = persistReducer(
  { storage, key: "demo1-auth", whitelist: ["user", "authToken", "RefreshToken"] },
  (state = initialAuthState, action) => {
    switch (action.type) {
      case actionTypes.Login: {
        const { authToken } = action.payload;

        return { authToken, user: undefined };
      }

      case actionTypes.GetPermissions: {
        let token = action.payload.token.authToken
        let RefreshToken = action.payload.token.RefreshToken
        var tokenData = jsonwebtoken.verify(token, config.JWTSECRET);
        var userObj = tokenData.user
        return { authToken: token, user: userObj, RefreshToken: RefreshToken };
      }

      case actionTypes.Register: {
        const { authToken } = action.payload;

        return { authToken, user: undefined };
      }

      case actionTypes.Logout: {
        // TODO: Change this code. Actions in reducer aren't allowed.
        return initialAuthState;
      }

      case actionTypes.UserLoaded: {
        const { user } = action.payload;
        return { ...state, user };
      }

      case actionTypes.setRefreshToken: {
          
          const RefreshToken  = action.payload.token;
          return { ...state , RefreshToken};

      }

      default:
        return state;
    }
  }
  
);

export const actions = {
  login: authToken => ({ type: actionTypes.Login, payload: { authToken } }),
  getPermissions: token => ({ type: actionTypes.GetPermissions, payload: { token }  }),
  setRefreshToken: token => ({ type: actionTypes.setRefreshToken, payload: { token }  }),
  register: authToken => ({
    type: actionTypes.Register,
    payload: { authToken }
  }),
  logout: () => ({ type: actionTypes.Logout }),
  requestUser: user => ({ type: actionTypes.UserRequested, payload: { user } }),
  fulfillUser: user => ({ type: actionTypes.UserLoaded, payload: { user } })
};

export function* saga() {
  yield takeLatest(actionTypes.Login, function* loginSaga() {
    yield put(actions.requestUser());
  });

  yield takeLatest(actionTypes.Register, function* registerSaga() {
    yield put(actions.requestUser());
  });

  yield takeLatest(actionTypes.UserRequested, function* userRequested() {
    const { data: user } = yield getUserByToken();

    yield put(actions.fulfillUser(user));
  });
}
