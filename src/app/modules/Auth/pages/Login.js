import React from "react";
import {connect} from "react-redux";
import {injectIntl} from "react-intl";
import * as auth from "../_redux/authRedux";
import {GetPermissions} from "../_redux/authCrud";
import * as RouteHelpers from '../../../../_metronic/_helpers/RouterHelpers'
import { useHistory } from "react-router-dom";

function Login(props) {
  const history = useHistory();
  var urlParams = RouteHelpers.get_uri_params(props.location)

  if(urlParams.hasOwnProperty('token') && urlParams.token !== ''){

    props.getPermissions({authToken: urlParams.token,RefreshToken:urlParams.RefreshToken});

  }
  else{

    GetPermissions().then(token=>{
      if(token.data.token === '')
        history.push('/error')
      else
        props.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken});
    })
  }
  return (
      <div className="login-form login-signin">
      </div>
  );
}

export default injectIntl(connect(null, auth.actions)(Login));
