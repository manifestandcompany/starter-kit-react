import axios from "axios";
export const APIURL = process.env.REACT_APP_API_ENDPOINT
export const PRODUCTS_URL = APIURL+"/comments";

// CREATE =>  POST: add a new comment to the server
export function createComment(post) {
  return axios.post(`${PRODUCTS_URL}/create`, { post });
}

// FETCH =>  POST: get all comments from the server
export function getComment(post) {
  return axios.post(`${PRODUCTS_URL}/get`, { post });
}

// DELETE =>  POST: delete comment from the server
export function deleteComment(commentId) {
  return axios.post(`${PRODUCTS_URL}/delete`, { commentId });
}

// UPDATE =>  POST: update comment status from the server
export function udpateCommentStatus(commentId, status) {
  return axios.post(`${PRODUCTS_URL}/udpateCommentStatus`, { commentId, status });
}