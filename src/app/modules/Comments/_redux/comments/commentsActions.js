import * as requestFromServer from "./commentsCrud";
import {commentsSlice, callTypes} from "./commentsSlice";
import * as auth from "../../../Auth/_redux/authRedux";
import {RefreshToken} from "../../../Auth/_redux/authCrud";

const {actions} = commentsSlice;

// CREATE COMMENTS
export const createComment = queryParams => dispatch => {
  
  dispatch(actions.startCall({ callType: callTypes.list }));
  dispatch(actions.startLoader({}));
  return requestFromServer
    .createComment(queryParams)
    .then(response => {

      const { comment } = response.data;
      dispatch(actions.commentCreated({ comment }));
    })
    .catch(error => {
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(createComment(queryParams))
        })
      }
      error.clientMessage = "Can't create Comment";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

// GET COMMENTS
export const fetchComments = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .getComment(queryParams)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.commentFetched({ totalCount, entities }));
    })
    .catch(error => {
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(fetchComments(queryParams))
        })
      }
      error.clientMessage = "Can't create Comment";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

// DELETE COMMENT
export const deleteComment = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .deleteComment(queryParams)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.commentDeleted({ totalCount, entities }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(deleteComment(queryParams))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      error.clientMessage = "Can't create Comment";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};


export const udpateCommentStatus = (id, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .udpateCommentStatus(id, status)
    .then(() => {
      dispatch(actions.commentStatusUpdated({ id, status }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(udpateCommentStatus(id, status))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      error.clientMessage = "Can't create Comment";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const updateMessage = () => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  dispatch(actions.commentsSuccessMessageUpdate());
};