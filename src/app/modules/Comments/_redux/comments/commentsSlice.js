import {createSlice} from "@reduxjs/toolkit";

const initialCommentState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  lastError: null,
  successMessage: ''
};
export const callTypes = {
  list: "list",
  action: "action"
};
export const commentsSlice = createSlice({
  
  name: "comments",
  initialState: initialCommentState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      state.permissionsError = `${action.payload.permissionsError}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
      state.actionsLoading = ''
      state.successMessage = ''
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getProductById
    productFetched: (state, action) => {
      state.actionsLoading = false;
      state.productForEdit = action.payload.productForEdit;
      state.error = null;
    },
    // fetchSignature
    signatureFetched: (state, action) => {
      state.actionsLoading = false;
      state.signature = action.payload.Signature;
      state.error = null;
    },
    // findProducts
    commentFetched: (state, action) => {

      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.signature = '';
      state.totalCount = totalCount;
    },
    // Get Product Detail With Comments
    productFetchedForView: (state, action) => {
      state.actionsLoading = false;
      state.productForView = action.payload.productForView;
      state.productComments = action.payload.comments;
      state.error = null;
    },
    // createComment
    commentCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.successMessage = 'Comment created, and will be visible after verification'
    },
    // start Loader
    startLoader: (state, action) => {
      state.actionsLoading = true;
      state.successMessage = ''
    },
    // commentsSuccessMessageUpdate
    commentsSuccessMessageUpdate: (state, action) => {
      state.actionsLoading = false;
      state.successMessage = '';
      state.permissionsError = '';
    },
    // updateProduct
    productUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.product.id) {
          return action.payload.product;
        }
        return entity;
      });
    },
    // deleteProduct
    commentDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deleteProducts
    productsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // productsUpdateState
    productsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
    // commentStatusUpdated
    commentStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
  }
});
