/* eslint-disable no-restricted-imports */
import React from "react";
import { Modal, Alert } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/comments/commentsActions";


export function CommnetsPermissionsDialog(show) {
  // Products Redux state
  let { errorMessage } = useSelector(
    (state) => ({ errorMessage: state.comments.permissionsError }),
    shallowEqual
  );
  const dispatch = useDispatch();
  const onHide = () => {
    dispatch(actions.updateMessage());
  }

  return (
    <div>
      {errorMessage && errorMessage!==null && errorMessage!=='' ?
        <Alert variant="danger" onClose={() => onHide() } dismissible>
          <Alert.Heading>Error!</Alert.Heading>
          <p>
            You do not have permissions to carry out this operation
          </p>
        </Alert>
        :
        ""
      }
    </div>

  );
}
