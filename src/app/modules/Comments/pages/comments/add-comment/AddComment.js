import React from "react"
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import * as actions from "../../../../Comments/_redux/comments/commentsActions";
import { shallowEqual, useSelector } from "react-redux";
import {CircularProgress} from "@material-ui/core";
import { Alert } from "react-bootstrap";


const ProductEditSchema = Yup.object().shape({
    comment: Yup.string()
      .required("comment is required"),
});

export function AddComment(props){
    let initComment = {
        postId: props.postId,
        comment: '',
        status: 'pending',
      };

    const { actionsLoading, createdStatus, permissionsError } = useSelector(
    (state) => ({
        actionsLoading: state.comments.actionsLoading,
        createdStatus: state.comments.successMessage,
        permissionsError: state.comments.permissionsError
    }),
    shallowEqual
    );

    const dispatch = useDispatch();

    // Create Comment
    const saveComment = (comment) => {
        dispatch(actions.createComment(comment));
    };
    const onHide = () => {
        dispatch(actions.updateMessage());
    }
    return (
        <div className="Comments whiteColor p3">
            <h4>Add Comment</h4>
            {createdStatus !== ''?
            <Alert variant="success" onClose={() => onHide() } dismissible>
                <span>
                    {createdStatus}
                </span>
            </Alert>
            :""}

            {permissionsError !== '' && permissionsError !== undefined?
            <Alert variant="danger" onClose={() => onHide() } dismissible>
                <span>
                    You do not have permissions to carry out this operation
                </span>
            </Alert>
            :""}


            <Formik
                enableReinitialize={false}
                initialValues={initComment}
                validationSchema={ProductEditSchema}
                onSubmit={(values) => {
                saveComment(values);
                }}
            >
            {({ handleSubmit }) => (
            <>
            <Form className="form form-label-right">
                <div className="form-group row">
                    <div className="col-lg-12">
                        <Field
                        rows={5}
                        name="comment"
                        as="textarea"
                        className="form-control"
                        />
                    </div>
                </div>
                <button
                    type="submit"
                    className="btn btn-primary ml-2 float-right"
                    onSubmit={() => handleSubmit()}
                >{actionsLoading === true ? <i className="fas fa-circle-notch fa-spin"></i> :'Save'}</button>
            </Form>
            </>
            )}
            </Formik>
        </div>
    );
}