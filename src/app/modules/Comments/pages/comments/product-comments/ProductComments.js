import React from "react";
import { shallowEqual, useSelector } from "react-redux";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import { deepPurple } from '@material-ui/core/colors';
import { AddComment } from '../add-comment/AddComment';

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
    purpleAvatar: {
        margin: 0,
        color: '#fff',
        backgroundColor: deepPurple[500],
        fontWeight: 600,
        textTransform: 'capitalize',
    },
    commentDetail:{
        fontSize: '1.1rem',
        color: '#464e5f',
        marginBottom: 0,
    },
    capitalize:{
        textTransform: 'capitalize',
    }
  }));

export function ProductComments(props) {

    const classes = useStyles();

    const { postCommentsList } = useSelector(
        (state) => ({
            postCommentsList: state.products.productComments,
        }),
        shallowEqual
    );
    
    const commentArray = list => {
        const listItems = list.map((comment, key) =>
            <div key={key}>
                <ListItem alignItems="flex-start">
                     <ListItemAvatar>
                         <Avatar className={classes.purpleAvatar}>{comment.name[0]}</Avatar>
                     </ListItemAvatar>
                     <ListItemText
                     primary={
                         <React.Fragment>
                             <h3 className={classes.capitalize}>{comment.name}</h3>
                         </React.Fragment>
                     }
                     secondary={
                         <React.Fragment>
                             <span className={classes.commentDetail}>{comment.detail}</span>
                         </React.Fragment>
                     }
                     />
                </ListItem>
                <Divider variant="inset" component="li" />
            </div>
        );
        return (
            <List className={classes.root}>{listItems}</List>
        )
    }
    return (
        <div className="Comments whiteColor p3">
            <h2>Comments</h2>
            {commentArray(postCommentsList)}
            <AddComment postId={props.id} />
        </div>
    );
}