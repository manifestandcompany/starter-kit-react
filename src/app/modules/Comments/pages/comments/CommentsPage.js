import React from "react"
import { Route } from "react-router-dom";
import { ProductsLoadingDialog } from "./products-loading-dialog/ProductsLoadingDialog"
import { ProductsCard } from "./ProductsCard"
import { ProductDeleteDialog } from "./product-delete-dialog/ProductDeleteDialog";
import { ProductStatusUpdateDialog } from "./product-status-update-dialog/ProductStatusUpdateDialog";
import { ProductsDeleteDialog } from "./products-delete-dialog/ProductsDeleteDialog";
import { ProductsUpdateStatusDialog } from "./products-update-status-dialog/ProductsUpdateStatusDialog";
import { ProductsUIProvider } from "./ProductsUIContext"
import { CommnetsPermissionsDialog } from "./commentss-permissions-dialog/CommentsPermissionsDialog"



export function CommentsPage({ history }) {
  const productsUIEvents = {
    
    openDeleteProductDialog: (id) => {
      history.push(`/comments/all/${id}/delete`);
    },
    openStatusProductDialog: (id,status) => {
      history.push(`/comments/all/${id}/${status}`);
    },
    openDeleteProductsDialog: () => {
      history.push(`/comments/all/deleteProducts`);
    },
    openFetchProductsDialog: () => {
      history.push(`/comments/fetch`);
    },
    openUpdateProductsStatusDialog: () => {
      history.push("/comments/all/updateStatus");
    },
  };
  return (
    <ProductsUIProvider productsUIEvents={productsUIEvents}>
      <CommnetsPermissionsDialog show={true} />
      <ProductsLoadingDialog />
      <Route path="/comments/all/:id/delete">
        {({ history, match }) => (
          <ProductDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/comments/all");
            }}
          />
        )}
      </Route>
      <Route path="/comments/all/:id/verify">
        {({ history, match }) => (
          <ProductStatusUpdateDialog
            show={match != null}
            id={match && match.params.id}
            status={"verify"}
            onHide={() => {
              history.push("/comments/all");
            }}
          />
        )}
      </Route>
      <Route path="/comments/all/:id/reject">
        {({ history, match }) => (
          <ProductStatusUpdateDialog
            show={match != null}
            id={match && match.params.id}
            status={"reject"}
            onHide={() => {
              history.push("/comments/all");
            }}
          />
        )}
      </Route>
      <Route path="/comments/all/deleteProducts">
        {({ history, match }) => (
          <ProductsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/comments/all");
            }}
          />
        )}
      </Route>
      <Route path="/comments/all/updateStatus">
        {({ history, match }) => (
          <ProductsUpdateStatusDialog
            show={match && match.params.id}
            onHide={() => {
              history.push("/comments/all");
            }}
          />
        )}
      </Route> 
      <ProductsCard />
    </ProductsUIProvider>
  );
}
