import React, { Suspense } from "react"
import { Redirect, Switch } from "react-router-dom"
import { CommentsPage } from "./comments/CommentsPage"
import { LayoutSplashScreen, ContentRoute } from "../../../../_metronic/layout"

export default function CommentPage() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from eCommerce root URL to /customers */
          <Redirect
            exact={true}
            from="/comments"
            to="/comments/all"
          />
        }
        <ContentRoute path="/comments/all" component={CommentsPage} />
      </Switch>
    </Suspense>
  );
}
