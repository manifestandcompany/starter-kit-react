import * as requestFromServer from "./productsCrud";
import {productsSlice, callTypes} from "./productsSlice";
import * as auth from "../../../Auth/_redux/authRedux";
import {GetPermissions, RefreshToken} from "../../../Auth/_redux/authCrud";


const {actions} = productsSlice;

export const fetchProducts = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findProducts(queryParams)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.productsFetched({ totalCount, entities }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(fetchProducts(queryParams))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't find products";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const fetchProduct = id => dispatch => {
  if (!id) {
    return dispatch(actions.productFetched({ productForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getProductById(id)
    .then(response => {
      const product = response.data;
      dispatch(actions.productFetched({ productForEdit: product }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(fetchProduct(id))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't find products";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const fetchProductDetailWithComments = id => dispatch => {
  if (!id) {
    return dispatch(actions.productFetched({ productForView: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getProductDetail(id)
    .then(response => {
      const post = response.data.post;
      const comments = response.data.comments;
      dispatch(actions.productFetchedForView({ productForView: post, comments: comments }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(fetchProductDetailWithComments(id))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't find products";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const fetchSignature = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .fetchSignature()
    .then(response => {
      const signature = response.data;
      dispatch(actions.signatureFetched({ Signature: signature }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(fetchSignature(queryParams))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't find signature";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const deleteProduct = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteProduct(id)
    .then(response => {
      dispatch(actions.productDeleted({ id }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(deleteProduct(id))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't delete product";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const createProduct = productForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createProduct(productForCreation)
    .then(response => {
      console.log(response)
      const { post } = response.data;
      dispatch(actions.postCreated({ post }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(createProduct(productForCreation))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't create product";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const updateProduct = product => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateProduct(product)
    .then(() => {
      dispatch(actions.productUpdated({ product }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(updateProduct(product))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't update products";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};


export const udpateProductStatus = (id, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .udpateProductStatus(id, status)
    .then(() => {
      dispatch(actions.productUpdatedStatus({ id, status }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(udpateProductStatus(id,status))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't update post status";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};


export const updateProductsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForProducts(ids, status)
    .then(() => {
      dispatch(actions.productsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(updateProductsStatus(ids, status))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't update statuses";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};

export const deleteProducts = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteProducts(ids)
    .then(() => {
      dispatch(actions.productsDeleted({ ids }));
    })
    .catch(error => {
      var permissionsError = ''
      if(error.message.indexOf('403') > -1){
        RefreshToken().then(token=>{
          dispatch(auth.actions.getPermissions({authToken: token.data.token, RefreshToken: token.data.RefreshToken}))
          dispatch(deleteProducts(ids))
        })
      }
      else if(error.message.indexOf('401') > -1){
        permissionsError = 'User Has Not Required Permissions'
      }
      else
      error.clientMessage = "Can't find products";
      dispatch(actions.catchError({ permissionsError, error, callType: callTypes.list }));
    });
};


export const updatePermissionError = () => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  dispatch(actions.productsErrorMessageUpdate());
};