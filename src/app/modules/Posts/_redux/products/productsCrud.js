import axios from "axios";
export const APIURL = process.env.REACT_APP_API_ENDPOINT
export const PRODUCTS_URL = APIURL+"/posts";

// CREATE =>  POST: add a new product to the server
export function createProduct(post) {
  return axios.post(`${PRODUCTS_URL}/create`, { post });
}

// READ
export function getAllProducts() {
  return axios.get(PRODUCTS_URL);
}

export function getProductById(postId) {
  return axios.post(`${PRODUCTS_URL}/getDetail`,{postId});
}

export function getProductDetail(postId) {
  return axios.post(`${PRODUCTS_URL}/getProductDetail`,{postId});
}


export function fetchSignature() {
  return axios.post(`${APIURL}/fetchSignature`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findProducts(queryParams) {
  return axios.post(`${PRODUCTS_URL}/get`, { queryParams });
}

// UPDATE => PUT: update the procuct on the server
export function updateProduct(post) {
  return axios.post(`${PRODUCTS_URL}/update`, { post });
  // return axios.put(`${PRODUCTS_URL}/${product.id}`, { product });
}

// UPDATE => PUT: update the procuct on the server
export function udpateProductStatus(postId, status) {
  return axios.post(`${PRODUCTS_URL}/updateStatus`, { postId, status });
  // return axios.put(`${PRODUCTS_URL}/${product.id}`, { product });
}


// UPDATE Status
export function updateStatusForProducts(ids, status) {
  return axios.post(`${PRODUCTS_URL}/updateMultipleStatuses`, {
    ids,
    status
  });
}

// DELETE => delete the product from the server
export function deleteProduct(postId) {
  return axios.post(`${PRODUCTS_URL}/delete`, {postId});
}

// DELETE Products by ids
export function deleteProducts(ids) {
  return axios.post(`${PRODUCTS_URL}/deleteMultiplePosts`, { ids });
}

// UPLOAD FILE
export function uploadFile(file) {
  const config = {
    headers: {
        'content-type': 'multipart/form-data'
    }
  };
  return axios.post(`${PRODUCTS_URL}/uploadFileToS3`, file, config );
}
