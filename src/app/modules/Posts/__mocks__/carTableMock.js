export default [
  {
    id: 1,
    title: 'post 1',
    description: 'post description 1',
    _userId: 1,
    _createdDate: "03/31/2015",
    _updatedDate: "05/08/2015"
  },
  {
    id: 2,
    title: 'post 2',
    description: 'post description 2 new',
    _userId: 2,
    _createdDate: "05/31/2015",
    _updatedDate: "07/08/2015"
  },
];
