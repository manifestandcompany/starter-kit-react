/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/products/productsActions";
import { ProductComments } from "../../../../Comments/pages/comments/product-comments/ProductComments"
import {
  Card,
  CardBody
} from "../../../../../../_metronic/_partials/controls";
import { ModalProgressBar } from "../../../../../../_metronic/_partials/controls";



export function ProductsViewDialog({
  match: {
    params: { id },
  },
}) {


  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, productForView } = useSelector(
    (state) => ({
      actionsLoading: state.products.actionsLoading,
      productForView: state.products.productForView
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchProductDetailWithComments(id));
  }, [id, dispatch]);

  return (
    <Card className="postView">
      {actionsLoading && <ModalProgressBar />}
      <CardBody>
          {productForView?
            <div className="boxShadow">
              <div className="whiteColor p3 mb-5">
                <h2>{productForView.title}</h2>
                <div className="content" dangerouslySetInnerHTML={{__html: productForView.description}}></div>
              </div>
              <ProductComments id={id} />
            </div>
          :""}
      </CardBody>
    </Card>
  );
}
