/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../../../_metronic/_helpers";


export const ActionsColumnFormatter = (
  cellContent,
  row,
  rowIndex,
  { openEditProductPage, openDeleteProductDialog,openStatusProductDialog, openViewProductDialog}
) => (
  <>
      <OverlayTrigger
        overlay={<Tooltip id="products-edit-tooltip">Edit</Tooltip>}
      >
        <a
          className="btn btn-icon btn-light btn-hover-primary btn-sm ml-1"
          onClick={() => openEditProductPage(row.id)}
        >
          <span className="svg-icon svg-icon-md svg-icon-primary">
            <SVG
              src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
            />
          </span>
        </a>
      </OverlayTrigger>
    
      <OverlayTrigger
        overlay={<Tooltip id="products-delete-tooltip">Delete</Tooltip>}
      >
        <a
          className="btn btn-icon btn-light btn-hover-danger btn-sm ml-1"
          onClick={() => openDeleteProductDialog(row.id)}
        >
          <span className="svg-icon svg-icon-md svg-icon-danger">
            <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
          </span>
        </a>
      </OverlayTrigger>

      { row.status==="publish" ?
      <>
      <OverlayTrigger
        overlay={<Tooltip id="products-delete-tooltip">Approve</Tooltip>}
      >
        <a
          className="btn btn-icon btn-light btn-hover-success btn-sm ml-1"
          onClick={() => openStatusProductDialog(row.id,'verify')}
        >
          <span className="svg-icon svg-icon-md svg-icon-success">
            <SVG src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")} />
          </span>
        </a>
      </OverlayTrigger>
      
      <OverlayTrigger
        overlay={<Tooltip id="products-delete-tooltip">Reject</Tooltip>}
      >
        <a
          className="btn btn-icon btn-light btn-hover-info btn-sm ml-1"
          onClick={() => openStatusProductDialog(row.id,'reject')}
        >
          <span className="svg-icon svg-icon-md svg-icon-info">
            <SVG src={toAbsoluteUrl("/media/svg/icons/Navigation/Close.svg")} />
          </span>
        </a>
      </OverlayTrigger>
      </>
      :
      ''
      }

      <OverlayTrigger
        overlay={<Tooltip id="products-delete-tooltip">View</Tooltip>}
      >
        <a
          className="btn btn-icon btn-light btn-hover-info btn-sm ml-1"
          onClick={() => openViewProductDialog(row.id)}
        >
          <span className="svg-icon svg-icon-md svg-icon-info">
            <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} />
          </span>
        </a>
      </OverlayTrigger>
  </>
);
