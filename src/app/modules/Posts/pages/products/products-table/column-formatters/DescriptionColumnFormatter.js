import React from "react";
const htmlToText = require('html-to-text');


export const DescriptionColumnFormatter = (cellContent, row) => (
  <div style={{overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: '1',
    WebkitBoxOrient: 'vertical',
    }}>
    {htmlToText.fromString(cellContent)}
  </div>
);
