/* eslint-disable no-restricted-imports */
import React from "react";
import { Modal, Alert } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/products/productsActions";


export function PostsPermissionsDialog(show) {
  // Products Redux state
  let { errorMessage } = useSelector(
    (state) => ({ errorMessage: state.products.permissionsError }),
    shallowEqual
  );
  const dispatch = useDispatch();
  const onHide = () => {
    dispatch(actions.updatePermissionError());
  }

  return (
    <div>
      {errorMessage && errorMessage!==null && errorMessage!=='' ?
        <Alert variant="danger" onClose={() => onHide() } dismissible>
          <Alert.Heading>Error!</Alert.Heading>
          <p>
            You do not have permissions to carry out this operation
          </p>
        </Alert>
        :
        ""
      }
    </div>

  );
}
