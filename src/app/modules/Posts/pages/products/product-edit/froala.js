import React, { Component } from 'react'
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import FroalaEditor from 'react-froala-wysiwyg';


import 'froala-editor/js/plugins.pkgd.min.js';


class froala extends Component {
  constructor(props){
    super(props)
    
    this.state = {
        model: props.description,
        value: '',
        preContent: 'Add a message',
        config : {}  
      };
    }

    componentDidMount(){
      this.props.updateDescription(this.props.description)
        this.setState({model: this.props.description, config: {
        name: "textarea",
        placeholderText: 'Edit Your Content Here!',
        colorsHEXInput: false,
        autoFocus: true,
        toolbartop: true,
        linkAlwaysBlank: true,
        fontFamilySelection: true,
        fontSizeSelection: true,
        paragraphFormatSelection: true,
        htmlExecuteScripts: true,
        iframe: true,
        tabSpaces: 4,
        toolbarButtons:{
          moreText: {
            buttons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', 'textColor', 'backgroundColor', 'inlineClass', 'inlineStyle', 'clearFormatting'],

            // Alignment of the group in the toolbar.
            align: 'left',

            // By default, 3 buttons are shown in the main toolbar. The rest of them are available when using the more button.
            buttonsVisible: 3
          },
          moreParagraph: {
            buttons: ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'paragraphFormat', 'paragraphStyle', 'lineHeight', 'outdent', 'indent', 'quote'],
            align: 'left',
            buttonsVisible: 3
          },

          moreRich: {
            buttons: ['insertLink', 'insertTable', 'emoticons', 'fontAwesome', 'specialCharacters', 'embedly', 'insertHR'],
            // buttons: ['insertLink', 'insertImage', 'insertVideo', 'insertTable', 'emoticons', 'fontAwesome', 'specialCharacters', 'embedly', 'insertFile', 'insertHR'],
            align: 'left',
            buttonsVisible: 3
          },

          moreMisc: {
            buttons: ['undo', 'redo', 'fullscreen', 'print', 'getPDF', 'spellChecker', 'selectAll', 'html', 'help'],
            align: 'right',
            buttonsVisible: 2
          }
        },
        // imageUploadToS3: this.props.signature,
        // videoUploadToS3: this.props.signature,
        // fileUploadToS3: this.props.signature,
        }
      })
    }
  

      handleModelChange = (model) => {
        this.setState( {
            model: model
        } );
        this.props.updateDescription(model)
      }
    render() {
        return(

               <div className="comment-reply">
                    
                      <div id="editor" className="form-group comment-reply-content">
                      {/* {this.state.config.hasOwnProperty('imageUploadToS3') ?  */}
                        <FroalaEditor
                          tag='textarea'
                          config = {this.state.config}
                          model={this.state.model}
                          onModelChange={this.handleModelChange}
                        />
                      {/* :
                        ""
                      } */}
                    </div>
                    
                </div>    
            
        )
    }
}
export default froala;