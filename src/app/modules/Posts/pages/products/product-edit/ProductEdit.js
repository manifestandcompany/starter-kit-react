/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/products/productsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../../_metronic/_partials/controls";
import { ProductEditForm } from "./ProductEditForm";
import { useSubheader } from "../../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../../_metronic/_partials/controls";



let initProduct = {
  id: undefined,
  title:'',
  description: '',
  status: '',
};

export function ProductEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("");

  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  let { actionsLoading, productForEdit, signature } = useSelector(
    (state) => ({
      actionsLoading: state.products.actionsLoading,
      productForEdit: state.products.productForEdit,
      signature: state.products.signature,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchProduct(id));
  }, [id, dispatch]);

  // useEffect(() => {
  //   dispatch(actions.fetchSignature());
  // }, [dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Post";
    if (productForEdit && id) {
      _title = `Edit Post '${productForEdit.title}'`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productForEdit, id]);

  const saveProduct = (id, title, description) => {
    if (!id) {
      dispatch(actions.createProduct({id: id, title:title, description: description, status: status})).then(() => backToProductsList());
    } else {
      dispatch(actions.updateProduct({id: id, title:title, description: description, status: status})).then(() => backToProductsList());
    }
  };

  const btnRef = useRef();  
  const publishProductClick = () => {
    setStatus('publish')
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };
  const draftProductClick = () => {
    setStatus('draft')
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backToProductsList = () => {
    history.push(`/posts/all`);
  };

  return (
    <Card>
      {actionsLoading && <ModalProgressBar />}
      <CardHeader title={title}>
        <CardHeaderToolbar>
          <button
            type="button"
            onClick={backToProductsList}
            className="btn btn-light"
          >
            <i className="fa fa-arrow-left"></i>
            Back
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={draftProductClick}
          >
            Save As Draft
          </button>
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={publishProductClick}
          >
            Publish
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProductEditForm
            actionsLoading={actionsLoading}
            postId={id}
            product={productForEdit || initProduct}
            // signature={signature}
            btnRef={btnRef}
            saveProduct={saveProduct}
          />
        </div>
      </CardBody>
    </Card>
  );
}
