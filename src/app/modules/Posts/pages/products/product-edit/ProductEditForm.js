// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, { useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../../_metronic/_partials/controls";
// import Froala from './froala';
import DraftEditor from './draftEditor';
import * as actions from "../../../_redux/products/productsCrud";

// Validation schema
const ProductEditSchema = Yup.object().shape({
  title: Yup.string()
    .required("Title is required"),
});

export function ProductEditForm({
  product,
  btnRef,
  postId,
  // signature,
  saveProduct,
}) {
  const [description, setDescription] = useState("")
  const [filesPath, setFilesPath] = useState("")
  const [loader, setLoader] = useState(false)
  const updateDescription = val => {
    setDescription(val)
  }


  const fileSelected = async (e) => {

    if(e.currentTarget.files.length){
      setLoader(true)
      let formData = new FormData();
      let fileType = e.currentTarget.files[0].type
      formData.append("uploadedFiles", e.currentTarget.files[0]);
      let url = await actions.uploadFile(formData)
      url = url.data
      let apiUrl = process.env.REACT_APP_API_ENDPOINT+"/getDownloadUrl?file="+url
      if(fileType.indexOf('image') === 0){
        url = '<img class=mb-3 style="max-width: 500px" src="'+apiUrl+'" />'
      }
      else if(fileType.indexOf('video') === 0){
        url = '<video class=mb-3 width="500" height="240" controls ><source src="'+apiUrl+'"></video>'
      }
      let filePathList = filesPath
      filePathList += (url + "<br />")
      setFilesPath(filePathList)
      setLoader(false)
  }

  }
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={product}
        validationSchema={ProductEditSchema}
        onSubmit={(values) => {
          let descriptionval = description + filesPath
          saveProduct(values.id, values.title, descriptionval);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="title"
                    component={Input}
                    placeholder="Title"
                    label="Title"
                  />
                </div>
              </div>
              
              <div className="form-group">
                <label>Description</label>
                {postId === undefined || (postId !== undefined && product.description) ?
                  <DraftEditor description={product.description} updateDescription={updateDescription} />
                  :
                  ""
                }
              </div>
              <div dangerouslySetInnerHTML={{__html: filesPath}}></div>
              <div className="form-group col-lg-4 d-flex pl-0">
                <div className="custom-file">
                  <input type="file" className="custom-file-input" id="customFile" onChange={event => fileSelected(event)} /> 
                  <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                </div>
                {loader === true ? <div className="col-lg-1 ml-3 spinner"></div> : ""}
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
