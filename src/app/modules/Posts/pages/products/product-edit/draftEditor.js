import React, { Component } from 'react'
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

class draftEditor extends Component {
    constructor(props){
        super(props)
        var contentBlock = ''
        if(props.description !== '' && props.description !== null && props.description !== undefined)
            contentBlock = htmlToDraft(props.description);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                model: editorState,
            };
        }
        else
        {
            this.state = {
                model: EditorState.createEmpty(),
            };
        }
    }
    handleModelChange = (model) => {
        this.setState( {
            model: model
        } );
        let ret_data = draftToHtml(convertToRaw(model.getCurrentContent()))
        this.props.updateDescription(ret_data)
    }
    render() {
        return(
            <Editor
            style={{border: '1px solid grey'}}
            editorState={this.state.model}
            toolbarClassName="toolbarClassName"
            wrapperClassName="draftEditorWrapper"
            editorClassName="editorClassName"
            onEditorStateChange={this.handleModelChange}
            />
        )
    }
}
export default draftEditor;