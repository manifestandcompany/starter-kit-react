import React from "react"
import { Route } from "react-router-dom";
import { ProductsLoadingDialog } from "./products-loading-dialog/ProductsLoadingDialog"
import { PostsPermissionsDialog } from "./posts-permissions-dialog/PostsPermissionsDialog"
import { ProductsCard } from "./ProductsCard"
import { ProductDeleteDialog } from "./product-delete-dialog/ProductDeleteDialog";
import { ProductStatusUpdateDialog } from "./product-status-update-dialog/ProductStatusUpdateDialog";
import { ProductsDeleteDialog } from "./products-delete-dialog/ProductsDeleteDialog";
import { ProductsUpdateStatusDialog } from "./products-update-status-dialog/ProductsUpdateStatusDialog";
import { ProductsUIProvider } from "./ProductsUIContext"



export function PostsPage({ history }) {
  const productsUIEvents = {
    newProductButtonClick: () => {
      history.push("/posts/new");
    },
    openEditProductPage: (id) => {
      history.push(`/posts/${id}/edit`);
    },
    openDeleteProductDialog: (id) => {
      history.push(`/posts/all/${id}/delete`);
    },
    openStatusProductDialog: (id,status) => {
      history.push(`/posts/all/${id}/${status}`);
    },
    openDeleteProductsDialog: () => {
      history.push(`/posts/all/deleteProducts`);
    },
    openFetchProductsDialog: () => {
      history.push(`/posts/fetch`);
    },
    openUpdateProductsStatusDialog: () => {
      history.push("/posts/all/updateStatus");
    },
    openViewProductDialog:(id)=>{
      history.push(`/posts/${id}/view`);
    }
  };

  return (
    <ProductsUIProvider productsUIEvents={productsUIEvents}>
      <PostsPermissionsDialog show={true} />
      <ProductsLoadingDialog />
      
      <Route path="/posts/all/:id/delete">
        {({ history, match }) => (
          <ProductDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/posts/all");
            }}
          />
        )}
      </Route>
      <Route path="/posts/all/:id/verify">
        {({ history, match }) => (
          <ProductStatusUpdateDialog
            show={match != null}
            id={match && match.params.id}
            status={"verify"}
            onHide={() => {
              history.push("/posts/all");
            }}
          />
        )}
      </Route>
      <Route path="/posts/all/:id/reject">
        {({ history, match }) => (
          <ProductStatusUpdateDialog
            show={match != null}
            id={match && match.params.id}
            status={"reject"}
            onHide={() => {
              history.push("/posts/all");
            }}
          />
        )}
      </Route>
      <Route path="/posts/all/deleteProducts">
        {({ history, match }) => (
          <ProductsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/posts/all");
            }}
          />
        )}
      </Route>
      <Route path="/posts/all/updateStatus">
        {({ history, match }) => (
          <ProductsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/posts/all");
            }}
          />
        )}
      </Route>
      <ProductsCard />
    </ProductsUIProvider>
  );
}
