import React, { Suspense } from "react"
import { Redirect, Switch } from "react-router-dom"
import { PostsPage } from "./products/PostsPage"
import { ProductEdit } from "./products/product-edit/ProductEdit"
import { ProductsViewDialog } from "./products/product-view/ProductsViewDialog"
import { LayoutSplashScreen, ContentRoute } from "../../../../_metronic/layout"

export default function postPage() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from eCommerce root URL to /customers */
          <Redirect
            exact={true}
            from="/posts"
            to="/posts/all"
          />
        }
        <ContentRoute path="/posts/:id/edit" component={ProductEdit} />
        <ContentRoute path="/posts/:id/view" component={ProductsViewDialog} />
        <ContentRoute path="/posts/new" component={ProductEdit} />
        <ContentRoute path="/posts/all" component={PostsPage} />
      </Switch>
    </Suspense>
  );
}
